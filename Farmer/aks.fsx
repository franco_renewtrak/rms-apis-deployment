#r "nuget: Farmer"

open Farmer
open Farmer.Builders


//let p = System.Diagnostics.Process.Start("cmd.exe",  """-c az aks create -g "sa-rg" -n "aks-cluster" --enable-managed-identity --generate-ssh-keys""")
//p.WaitForExit()
//az aks create -g "sa-rg" -n "aks-cluster" --enable-managed-identity --generate-ssh-keys


let pool = agentPool {
    name "ausr17"
    count 3
}


let aksCluster = aks {
    name "aks-cluster"
    enable_rbac
    service_principal_use_msi
    add_agent_pool pool
}

let deployment = arm {
    location Location.AustraliaEast
    add_resource aksCluster
}

deployment |> Deploy.execute "sa-rg" Deploy.NoParameters
