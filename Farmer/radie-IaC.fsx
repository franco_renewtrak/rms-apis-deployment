// Script to automate Azure resources infrastructure provisioning (use dotnet fsi)
#r "nuget: Farmer"

open System
open Farmer
open Farmer.Builders
open Farmer.ContainerService

[<Literal>]
let defaultResourceGroup = "rmsrtsaas-au-uat-rg"
[<Literal>]
let defaultCluster = "rt-au-uat-radie-cluster"
[<Literal>]
let defaultNodesNumber = 3

//let env = fsi.CommandLineArgs.[1]

let resourceGroup = "sa-rg"//defaultResourceGroup
let cluster = defaultCluster
let poolName = "radiepool"
let nodes = defaultNodesNumber

let pool = agentPool {
    name poolName
    count nodes
}

let radieAKSCluster = aks {
    name cluster
    dns_prefix "radiedns"
    service_principal_use_msi
    add_agent_pool pool
}

let myStorageAccount = storageAccount {
    name "pederobba123123"
}

//let deployment = arm {
//    location Location.AustraliaEast
//    add_resource myStorageAccount
//}

let deployment = arm {
    location Location.AustraliaEast
    add_resource radieAKSCluster
}

deployment |> Deploy.execute resourceGroup Deploy.NoParameters
